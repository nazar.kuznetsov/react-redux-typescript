import React, {Component} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';

export class Trash extends Component {
  render() {
    return (
      <div>
        Trash
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(Trash);
