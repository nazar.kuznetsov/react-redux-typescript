import {INBOX_READ_MESSAGE, INBOX_DELETE_MESSAGE, INBOX_ADD_MESSAGE} from './constans';

export const readMessage = id => {
  return {
    type: INBOX_READ_MESSAGE,
    payload: {id}
  };
};

export const deleteMessage = id => {
  return {
    type: INBOX_DELETE_MESSAGE,
    payload: {id}
  };
};

export const addMessage = message => {
  return {
    type: INBOX_ADD_MESSAGE,
    payload: message
  };
};
