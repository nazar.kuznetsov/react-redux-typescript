import {INBOX_READ_MESSAGE, INBOX_DELETE_MESSAGE, INBOX_ADD_MESSAGE} from './constans';

const initialState = {
  messagesList: ['trash']
};

export default (state = initialState, action) => {
  switch (action.type) {
    case INBOX_READ_MESSAGE:
      return {
        ...state, messagesList: state.messagesList.map(message => {
          if (message.id === action.payload.id) {
            message.read = true;
          }
          return message;
        })};
    case INBOX_DELETE_MESSAGE:
      return {
        ...state, messagesList: state.messagesList.filter(message => message.id !== action.payload.id)
      };
    case INBOX_ADD_MESSAGE:
      return {
        ...state,
        messagesList: [...state.messagesList, action.payload].sort((a, b) => b.time.getTime() - a.time.getTime())
      };
    default: return state;
  }
};

