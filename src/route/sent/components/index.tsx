import React, {Component} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';

export class Sent extends Component {
  render() {
    return (
      <div>
        Sent
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(Sent);
