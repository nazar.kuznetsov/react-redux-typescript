import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {fetchUser, signIn} from '../../auth/actions';
import {withRouter} from 'react-router-dom';
import logo from './logo.png';

class Login extends Component {
  componentWillMount() {
    this.props.fetchUser();

    if (this.props.authenticated) {
      this.props.history.push(this.props.location.state ? this.props.location.state.from : '/');
    }
  }

  componentWillUpdate(nextProps) {
    if (nextProps.authenticated) {
      this.props.history.push(this.props.location.state ? this.props.location.state.from : '/');
    }
  }

  render() {
    if (this.props.authenticated) {
      return null;
    }
    return (
      <>
      {this.props.authenticated === null
        ? <div className="login-page">
          <div className="login-page__wellcome">
            <img className="login-page__logo"
              src="https://upload.wikimedia.org/wikipedia/commons/4/4e/Gmail_Icon.png" alt="logo"
            />
            <h1 className="login-page__title">Добро пожаловать</h1>
            <button onClick={this.props.signIn}>
              <img src={logo} alt="img"/>
             Google
            </button>
          </div>
        </div>
        : null}
      </>

    );
  }
}

const mapStateToProps = state => ({
  authenticated: state.auth
});


export default connect(mapStateToProps, {fetchUser, signIn})(withRouter(Login));
