import React, {Component} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';

export class Spam extends Component {
  render() {
    return (
      <div>
        Spam
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(Spam);
