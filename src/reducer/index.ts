import {combineReducers} from 'redux';
import {reducer as inbox} from '../route/home';
import {reducer as sent} from '../route/sent';
import {reducer as spam} from '../route/spam';
import {reducer as trash} from '../route/trash';
// import {firestoreReducer} from 'redux-firestore';
// import {auth} from '../auth/reducer';

export default combineReducers({
// firestoreReducer,
  inbox,
  sent,
  // auth,
  spam,
  trash
});
