import React, {Component, Suspense} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Header} from '../components/header';
import 'reset-css';

const Home = React.lazy(() => import('../route/home/components'));
const Sent = React.lazy(() => import('../route/sent/components'));
const Trash = React.lazy(() => import('../route/trash/components'));
const Spam = React.lazy(() => import('../route/spam/components'));
const NotFound = React.lazy(() => import('../route/404'));
// const Login = React.lazy(() => import('../route/login'));

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Header />
        <Suspense fallback={null}>{/* индикатор загрузки */}
          <Switch>
            <Route exact={true} path="/" component={Home} />
            <Route exact={true} path="/sent" component={Sent} />
            <Route exact={true} path="/spam" component={Spam} />
            <Route exact={true} path="/trash" component={Trash} />
            {/* <Route exact={true} path="/login" component={Login} /> */}
            <Route component={NotFound} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    );
  }
}


export default App;
